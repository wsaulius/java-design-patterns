package command.example;

public class Controler {

    // Instantiate with generic (interface): all objects that are implementing CommandInterface are fine:
    // our BubbleSort and HeapSort algorithms are injected here
    private CommandInterface command;

    public void setCommand(CommandInterface command) {
        this.command = command;
    }

    public void runSort() {
        command.execute();
    }

}
