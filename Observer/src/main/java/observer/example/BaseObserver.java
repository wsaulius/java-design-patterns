package observer.example;

public abstract class BaseObserver {

    protected final observer.example.ChatChannel chatChannel;

    public BaseObserver(final ChatChannel chatChannel) {
        this.chatChannel = chatChannel;
        chatChannel.subscribe(this);
    }

    public abstract void handleMessage(String message, String userType);

    public abstract String getObserverType();

    public void sendMessage(final String message) {
        chatChannel.sendMessage(message, getObserverType());
    }
}
