package decorator.example;

public class HamPizza extends PizzaDecorator {

    public HamPizza(Pizza pizza) {
        super(pizza);
        super.addIngredients("Ham");
    }

}