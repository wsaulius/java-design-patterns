package decorator.example;

import netscape.javascript.JSObject;

import java.util.ArrayList;
import java.util.Collection;

public final class BasicPizza implements Pizza {
    ArrayList<String> ingredients;

    Collection<CardProcessingHandler> allJsonHandlers;

    public BasicPizza() {
        ingredients = new ArrayList<>();
        addIngredients("Pizza dough");
        addIngredients("Tomato Sauce");
        addIngredients("Cheese");
    }

    @Override
    public void printIngredients() {
        System.out.println("Pizza: "+String.join(", ", ingredients));
    }

    @Override
    public void addIngredients(String ingredient) {
        ingredients.add(ingredient);
    }
}