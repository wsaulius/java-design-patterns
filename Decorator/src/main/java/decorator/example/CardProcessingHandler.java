package decorator.example;

import netscape.javascript.JSObject;

public interface CardProcessingHandler {

    public void handle(final JSObject json);
}
