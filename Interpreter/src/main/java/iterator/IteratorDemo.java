package iterator;

import java.util.Iterator;

public class IteratorDemo {

    public static void main(String[] args) {

        RangeIterator iterator = new RangeIterator();

        Iterator ii = iterator.range(10, 20);

        while (ii.hasNext()) {
            System.out.println(ii.next());
        }

        // or using a lambda
        // iterator.forEachRemaining(System.out::println);
    }

}