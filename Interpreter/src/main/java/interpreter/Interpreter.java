package interpreter;

import java.util.HashMap;
import java.util.Map;
import interpreter.example.Expression;
import interpreter.example.*;

public class Interpreter {

    public static void main(String[] args) {

        // Build the expression tree
        Expression expression = new Plus(new Variable("a"), new Variable("b"));

        // Define the variables
        Map<String, Expression> variables = new HashMap<>();
        variables.put("a", new Variable("a"));
        variables.put("b", new Variable("b"));

        // Interpret the expression
        int result = expression.interpret(variables);
        System.out.println(result);
    }

}