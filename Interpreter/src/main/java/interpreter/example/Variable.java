package interpreter.example;

import java.util.Map;

// The variable expression
public class Variable implements Expression {
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    public int interpret(Map<String, Expression> variables) {
        if (variables.containsKey(name)) {

            // Recursion unexpected
            return variables.get(name).interpret(variables);
        }
        return 0;
    }
}
