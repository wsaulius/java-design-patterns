package proxy;

import proxy.example.SecurityProxy;
import proxy.example.TwitterService;
import proxy.example.TwitterServiceStub;

public class TwitterDemo {
    public static void main(String ... args){
        TwitterService service = (TwitterService) SecurityProxy.newIntance(new TwitterServiceStub());

        System.out.println(service.getTimeline("wherever"));
    }
}
