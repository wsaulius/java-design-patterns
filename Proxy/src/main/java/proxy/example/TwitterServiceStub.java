package proxy.example;

import proxy.example.TwitterService;

public class TwitterServiceStub implements TwitterService {

    @Override
    public String getTimeline(String screenName) {
        return "My little timeline";
    }

    @Override
    public void postToTimeline(String screenName, String message) {

    }
}
